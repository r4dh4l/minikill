**miniK - a [Warhammer 40k](https://de.wikipedia.org/wiki/Warhammer_40.000) Kill Team (2018 release) adjustment for a faster and more balanced skirmish (tournament) experience**

This REAMDE contains the whole rule set.

**Attention**: If you look for a similar rule set for Kill Team 2 (2021 release), try [miniK2](https://gitlab.com/r4dh4l/minik2).

Adapt the standard rules published by Games Workshop by the following:

# Command Roster

- Max. units: 20
- To avoid writing data cards use: [ruKTTCR : r4dh4l‘s ultimate KILL TEAM TOURNAMENT COMMAND ROSTER](https://gitlab.com/r4dh4l/rukttcr/)

# Battle-Forged Kill Team criteria

- Unit minimum for a Battle-Forged Kill Team is 1, not 3 (so you can form up with an Allarus Custodian only).
- Max. points: `67` (so an Allarus Custodian is still possible to play)
- Specialist maximum is "overall 3 Specialists" instead of "Leader + 3 Specialists" (so the proportion of the amount specialists for Adeptus Custodian in relation to other teams is the same compared with 100 point team sizes)
- 1 of 3 specialists may be a Leader but doesn't need to be (so that you can form up with an Allarus Custodian Zealot only if you want)

Reason/idea: For casual players a Kill Team match will last much longer than one hour playing with 100 points or even 125 points. Especially for tournaments with 3 matches it is hard to play 3 matches if the majority of participants are not able to finish a match in around 1 hour. The only way to keep the complexity of the game and to reduce the duration of a match for casual players is to reduce the team size to a mininum which still allows all factions to participate (which is 67 points for an Allarus Custodian at the moment).

# Factions

- everything released until KT Annual 2019
- with Sisters of Silence (White Dwarf 454)

# Sub-Factions

Kill Teams which don't have any official Sub-Factions or without a Sub-Faction equivalent Elite ability (Kroot, Adepta Sororitas etc.) can choose a single Sub-Faction from any existing official Sub-Faction or a Sub-Faction equivalent Elite ability. Some examples:

- Adepta Sororitas team with the Tau Sacea-Sept ability (+1 on hit rolls, +1 Morale)
- Servants of the Abyss with Grey Knights' "Brotherhood of Psykers" ability (2 PSY attacks)
- Kroot with Deathwatch "Mission Tactics" to re-roll wound rolls of 1 for a selected unit type

Reason/idea: Not all factions got Sub-Factions (like Adepta Sororitas or Kroot) or a Sub-Faction equivalent ability (means anything like Grey Knights "Brotherhood of Psykers") so these Factions will hardly participate in tournaments. Because the affected factions are not strong in general at all it is furthermore a little boost for them giving them access to all kind of existing Sub-Factions or equivalent abilities. Another option would be "no Sub-Faction (abilities) for any faction at all" but this seems less fun for everyone.

# Misc

- without Kill Team dispositions (White Dwarf 450)
- Specialist levels above level 1 can be used according to the core rules p66.

- Optional idea: Unit spam limiter (against Drone/Wrack/Plasma spam): For unit types which allow any kind of modifications (by wargear or sub-types like `<UnitType> Gunner`) you are only allowed to take one unit with the same wargear configuration. Example: A T'au Empire team can contain as much Drones as usual but only one may be a Gun Drone. Counterexample: A Necron team can have as much Necron warriors as usual with the same wargear configuration because the are not affected by this rule because the unit type "Necron warrior" can not be modificated in any way (neither by wargear nor by sub-type).

# Game mechanic changes

## Broken test

Instead of testing for a broken Kill Team when more than the half of your units are out of action or shaken the test is necessary if the *point value* of such units is more than 50%.

Reason/idea: Same reason as for holding objectives and "took out of action more units" criteria.

## Team size deviation in Command Points

If your Battle-Forged Kill Team has less points than your opponent's Kill Team you get the point difference in Command Points, example: a Kill Team with 65 points which is matched with a Kill Team of 66 points will start with 1 more Command Point.

Reason/idea: The standard rule to balance point differences provides 1CP per 10p differenze which doesn't work because nobody will waive 10p for units for just 1 cp.

## Door handling

1. A regular movement doesn't end closing/opening a door but just reduces the regular movement range of the unit by 1".
2. A unit can open/close multiple doors or try to prevent opening/closing a door multiple times this way as long it still has enough of its regular mv range left.
3. A door can be destroyed by attacks: +1 hit bonus to hit the door, profile of the door: T 5, W 1, no Save roll (the door has no body that is protected by an amor, body and armor the same) but collects "flesh wounds" as regular units; when destroyed a door becomes difficult terrain to pass the gap (tip over the door to "landscape mode" to indicate this).

Reason/idea: The regular door handling concept of Arena prevents participation of small teams because you would need extra units just to open doors to get on objective or attack range.

## Victory Points conditions

- +1 VP holding one and another +1 VP holding more objective markers than your opponent
- +1 VP for taking out of action one unit and another +1 VP for taking out of action more units units than your opponent

Reason/idea: Standard rule set tends to support strategies on holding objectives more than strategies to win by kills. A hold1/more and kill1/more concept (introduced in LVO/Las Vegas Open tournament?) is a very good solution for this problem.

### Holding objectives criteria

The criteria for holding objectives (or other positions) is not the unit count but the sum of unit points trying to hold the objective (so you can not hold an objective marker with 2 poxwalkers (2x3p) if there is a Custodian Guard (33p) in range as well).

Reason/idea: Does it make sense that 2 poxwalkers for 6p will hold an objective against a Termi? Not really. Counting just the number of units seems to be a "let's keep it very simple" rule decision.

### "Took out of action more units" criteria

Calculating who has taken more units out of action count the overall unit costs, not the number of units.

Reason/idea: Is it fair that taking out of action a 20p unit counts as same success as taking out of action a 3p unit? Counting just the number of units seems to be a "let's keep it very simple" rule decision.

## Reserve

Not only up to 50% of your units can be set up in reserve but as much as you want (even all of your units).

Reason/idea: Some tactics allow to enter the battlefield with up to 3 models which started as reserve units. The standard rules allow only up to 50% units starting in reserve. With 67p some Kill Teams can not even place 3 units in reserve without breaking the 50% rule so these tactics become useless. Dropping the 50% rule invites to drop any percentage rule: Although the name "reserve" doesn't make sense anymore if someone starts with all units in reserve the case of two players start completely in reserve is not a show-stopper for an interesting match.
